" Platform Identification
silent function! Platform(plat)
  if a:plat == 'osx'
    return has('macunix')
  elseif a:plat == 'linux' 
    return has('unix') && !has('macunix') && !has('win32unix')
  elseif a:plat == 'windows' 
    return (has('win16') || has('win32') || has('win64'))
  elseif a:plat == 'unixlike' 
    return !(has('win16') || has('win32') || has('win64'))
  elseif a:plat == 'freebsd' 
    let s:uname = system('uname -s')
    return (match(s:uname, 'FreeBSD') >= 0)
  endif
endfunction

" Autoinstalls Vim-Plug if it isn't installed
if Platform('linux') && 
      \ empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
elseif Platform('windows') && 
      \ empty(glob('~/AppData/Local/nvim/autoload/plug.vim'))
  silent !powershell -Command
        \ (New-Object Net.WebClient).DownloadFile(
        \   'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
        \   $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath(
        \     '~/AppData/Local/nvim/autoload/plug.vim'
        \   )
        \ )
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Vim Plug
if Platform('linux')
  let vimplugroot = '~/.local/share/vim/plug'
elseif Platform('windows')
  let vimplugroot = '~/AppData/Local/nvim/plug'
endif

call plug#begin(vimplugroot)
  Plug 'tpope/vim-obsession'
  Plug 'preservim/nerdtree'
  Plug 'tpope/vim-commentary'
  Plug 'unblevable/quick-scope'
  Plug 'tpope/vim-surround'
  Plug 'junegunn/vim-easy-align'
  Plug 'chrisbra/Colorizer'
  Plug 'tpope/vim-repeat'
  Plug 'kreskij/Repeatable.vim', { 'on': 'Repeatable' }
  " Themes
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'artanikin/vim-synthwave84'
  Plug 'ryanoasis/vim-devicons'
  " Languages
  Plug 'bhurlow/vim-parinfer', { 'for': ['clojure', 'scheme'] }
  " Plug 'kovisoft/paredit', { 'for': ['clojure', 'scheme'] } 
  Plug 'plasticboy/vim-markdown', { 'for': ['markdown'] }
  Plug 'dkarter/bullets.vim', { 'for': ['markdown'] }
  " Requisites
  Plug 'godlygeek/tabular', { 'for': ['markdown'] } " Required by vim-markdown
call plug#end()

" ### MAPPINGS ###
let mapleader = ' '

map ; :Commentary <CR>
nmap <C-N><C-N> :set invnumber<CR>

" ## Leader Keybinds ## 
map <leader>w :w<cr>
map <leader>W :wqa<cr>
map <leader>q :qa<cr>
map <leader>Q :qa!<cr>

" xmap: bind in visual mode (e.g. vipga). 
" nmap: bind for motion/textobject (e.g. gaip)
xmap <leader>a <Plug>(EasyAlign)
nmap <leader>a <Plug>(EasyAlign)

" Switch windows (Source: https://neovim.io/doc/user/nvim_terminal_emulator.html)
tnoremap <A-Left> <C-\><C-N><C-w>h
tnoremap <A-Down> <C-\><C-N><C-w>j
tnoremap <A-Up> <C-\><C-N><C-w>k
tnoremap <A-Right> <C-\><C-N><C-w>l
inoremap <A-Left> <C-\><C-N><C-w>h
inoremap <A-Down> <C-\><C-N><C-w>j
inoremap <A-Up> <C-\><C-N><C-w>k
inoremap <A-Right> <C-\><C-N><C-w>l
nnoremap <A-Left> <C-w>h
nnoremap <A-Down> <C-w>j
nnoremap <A-Up> <C-w>k
nnoremap <A-Right> <C-w>l

" Switch buffers
nmap <silent> <C-PageUp>  :bp<CR>
nmap <silent> <C-PageDown> :bn<CR>

" Friendly Terminal Commands (Source: https://neovim.io/doc/user/nvim_terminal_emulator.html)
cabbrev vterm Vterm
cabbrev hterm Hterm
command Vterm vsplit | wincmd l | term
command Hterm split | wincmd j | term
tnoremap <Esc> <C-\><C-n>
tnoremap <expr> <C-R> '<C-\><C-N>"'.nr2char(getchar()).'pi'
tmap <S-Insert> <C-R>+

" #### THEMES / THEMEING ####
" Airline (Source: https://vi.stackexchange.com/a/3363)
" Airline Fonts
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
" arrow
let g:airline_left_sep      = "\uE0B0" " 
let g:airline_left_alt_sep  = "\uE0B1" " 
let g:airline_right_sep     = "\uE0B2" " 
let g:airline_right_alt_sep = "\uE0B3" " 
" " semicircle
" let g:airline_left_sep      = "\uE0B4" 
" let g:airline_left_alt_sep  = "\uE0B5" 
" let g:airline_right_sep     = "\uE0B6" 
" let g:airline_right_alt_sep = "\uE0B7" 
" " slant
" let g:airline_left_sep      = "\uE0B8"
" let g:airline_left_alt_sep  = "\uE0B9"
" let g:airline_right_sep     = "\uE0BA"
" let g:airline_right_alt_sep = "\uE0BB"

let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = '' 

" Airline Configuration
" let g:airline_statusline_ontop=1
let g:airline#extensions#tabline#enabled=1
let g:airline_detect_crypt=1
let g:airline_detect_paste=1
let g:airline_detect_spell=1

" QuickScope (for highlighting character motions)
let g:qs_buftype_blacklist = ['terminal', 'nofile']
augroup qs_colors
  autocmd!
  autocmd ColorScheme * highlight QuickScopePrimary guifg='#afff5f' gui=underline ctermfg=155 cterm=underline
  autocmd ColorScheme * highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=81 cterm=underline
augroup END

" Theme / Colors
" https://github.com/vim-airline/vim-airline/wiki/Screenshots
set laststatus=2 termguicolors " all windows should have status lines, display w/ termgui colors
colorscheme synthwave84
" let g:airline_theme = 'deus'
" let g:airline_theme = 'bubblegum'
" let g:airline_theme = 'luna'
let g:airline_theme = 'violet'

" ### EDITOR BEHAVIOR ###
filetype plugin on
" Nerdtree autoexist 
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * call VimEnterNerdTree()

fun VimEnterNerdTree()
  if argc() == 1 && !exists("s:std_in") && isdirectory(argv()[0])
    exe 'cd ' . argv()[0]
    NERDTree | wincmd p | ene
  elseif argc() == 0 && !exists("s:std_in") 
    NERDTree | wincmd p
  endif
endfun

" Set a character limit column
set colorcolumn=80
" set textwidth=80 " Set auto wrapping

set number " display line numbers
" " ident by an additional 2 characters on wrapped lines, when line >= 40 characters, put 'showbreak' at start of line
" set breakindent breakindentopt=shift:2,min:40,sbr formatoptions=l linebreak showbreak=.. 
set breakindent formatoptions=l linebreak " break indent / smart wrap
" set conceallevel=2 " Conceal code, wrap code, etc at level 2

" Indentation [Source]: https://superuser.com/a/1104409
set tabstop=2 softtabstop=-1 shiftwidth=0 expandtab

" Set Shell
if Platform('linux')
  set shell=/bin/bash
elseif Platform('windows')
  set shell=powershell.exe
  set shellcmdflag=-NoProfile\ -NoLogo\ -NonInteractive\ -Command
  set shellpipe=|
  set shellredir=>
endif

" Set Colorizer stuff
let g:colorizer_disable_bufleave = 1
" let g:colorizer_auto_color = 1
" let g:colorizer_auto_filetype='scss,css,clojure,xdefaults'

" #### LANGUAGE SPECIFIC ####
" Markdown
let g:vim_markdown_math = 1
let g:vim_markdown_strikethrough = 1
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_new_list_item_indent = 0
let g:bullets_enabled_file_types = [
    \ 'markdown',
    \ 'text',
    \ 'gitcommit',
    \ 'scratch'
    \]
let g:bullets_enable_in_empty_buffers = 1
let g:bullets_checkbox_markers = ' ---x' " Binary
" let g:bullets_checkbox_markers = ' .oOX' " Graded

" Other
autocmd Filetype sh setlocal tabstop=2
autocmd Filetype clj setlocal tabstop=2
