# MyriaCore's nvim dotfiles

Here, I have dotfiles for nvim and [fvim](https://github.com/yatli/fvim). 

I have since modified my configurations to detect platform automatically. 

On windows, the default shell should be powershell, and configs should be in `~\AppData\Local\nvim`.
